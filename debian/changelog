pypuppetdb (3.2.0-1) unstable; urgency=medium

  * Team upload
  * New upstream version 3.2.0 (Closes: #1052771)
  * add gbp.conf
  * drop dependency on old python3-mock
  * use new dh-sequence-python3
  * refresh 0003 patch

 -- Alexandre Detiste <tchet@debian.org>  Sat, 20 Jan 2024 15:56:54 +0100

pypuppetdb (2.2.0-1) unstable; urgency=medium

  * Team upload

  [ Louis-Philippe Véronneau ]
  * New upstream release.
  * d/control: update to dh13.
  * d/patches: remove 0001 (not needed) and add 0002 to not to run coverage
    during tests. (Closes: #973137)
  * d/patches: add 0003 to not install unrelated files.
  * d/rules: Don't tweak dh_auto_install, we don't need to do that anymore
    since we're not running coverage tests.
  * d/patches: add 0004 to fix version.
  * d/control: Standards-Version update to 4.5.1. Add Rules-Requires-Root.
  * d/watch: update to v4 syntax and use git mode.
  * d/copyright: update to reflect the move to Vox Pupuli.
  * d/tests: run the upstream testsuite as an autopkgtest.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Fri, 25 Dec 2020 15:10:17 -0500

pypuppetdb (0.3.3-2) unstable; urgency=medium

  * Team upload.
  * d/copyright: Use https protocol in Format field.
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.
  * Drop Python 2 support.
  * Enable autopkgtest-pkg-python testsuite.
  * Bump debhelper compat level to 12.

 -- Ondřej Nový <onovy@debian.org>  Thu, 08 Aug 2019 16:40:38 +0200

pypuppetdb (0.3.3-1) unstable; urgency=medium

  * New upstream version 0.3.3
  * Add myself to Uploaders
  * d/control: update Homepage URL
  * d/watch: Use Github
  * d/watch: ignore bogus upstream version 3.3.3
  * Fix tests (Closes: #815393)
    + Drop B-D on python-tox to force using pytest
    + B-D on python-bandit and run the bandit tests
    + B-D on python-cov
  * override_dh_auto_test: test for `nocheck' in DEB_BUILD_OPTIONS
  * Bump Standards-Version to 4.3.0; no changes needed
  * d/control: use HTTPS for Vcs-*
  * Fix header name case for Python 3
  * Disable tests for Python 2
  * d/control: drop X-Python*-Version fields
  * Bump dh compat to 11; no changes needed
  * Switch Vcs-* URLs to salsa.d.o
  * d/control: fix typo in description

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Mon, 11 Feb 2019 22:59:56 +0200

pypuppetdb (0.1.1+git080614-1) unstable; urgency=low

  * Initial release. (Closes: #756009)

 -- Jonas Genannt <genannt@debian.org>  Thu, 04 Jun 2015 12:43:48 +0200
